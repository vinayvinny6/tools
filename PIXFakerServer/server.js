// BASE SETUP
// =============================================================================

// call the packages we need
var express    = require('express');        // call express
var app        = express();                 // define our app using express
var bodyParser = require('body-parser');

var UserDetails= require('./app/models/userDetails');

// configure app to use bodyParser()
// this will let us get the data from a POST
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var port = process.env.PORT || 7123;        // set our port


// ROUTES FOR OUR API
// =============================================================================
var router = express.Router();              // get an instance of the express Router

// middleware to use for all requests (used for logging)
router.use(function(req, res, next) {
    console.log('************************** NEW REQUEST **************************');
    var today = new Date();
    var time = ("0" + today.getHours()).slice(-2) + ":" +
               ("0" + today.getMinutes()).slice(-2) + ":" +
               ("0" + today.getSeconds()).slice(-2);
    console.log('Time:', time);

    next(); // make sure we go to the next routes and don't stop here
});

// test route to make sure everything is working (accessed at GET http://localhost:8123/generate)
router.get('/', function(req, res) {
    res.json({ message: 'This route is empty, try /generate/userDetails' });   
});

// ----------------------------------------------------
router.get('/userDetails', function(req, res) {
    var user = UserDetails.create();

    console.log('  ' + req.method + ' ' + req.url + ': Creating UserDetails');
    console.log('    UserDetails: ' + user.firstName +" "+ user.lastName + ",  " + user.email + ", " + user.phone);

    res.send(user); 
});




// REGISTER OUR ROUTES -------------------------------
// all of our routes will be prefixed with /generate
app.use('/generate/', router);

// START THE SERVER
// =============================================================================
app.listen(port);
console.log('Magic happens on port ' + port);