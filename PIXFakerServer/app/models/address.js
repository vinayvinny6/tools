var faker = require('faker');
var addresses = require('../data/addresses.json');
faker.locale = "en_US";

function Address(prefFlag) {
    this.streetAddress = faker.address.streetAddress(),
    this.streetAddress2 = faker.address.secondaryAddress(),
    this.streetAddress3 = faker.address.county(),
    this.city= faker.address.city(),
    this.state= faker.address.stateAbbr(),
    this.postalCode= faker.address.zipCode("#####"),
    this.countryCode= "USA",
    this.prefAddressFlag= prefFlag;
}

function create(prefFlag) {
    var randomNum = Math.floor(Math.random()*14) + 1;
    var addressVal = addresses[randomNum];
    addressVal.prefAddressFlag = prefFlag;
    addressVal.streetAddress2 = faker.address.secondaryAddress();
    addressVal.streetAddress3 = faker.address.county();
    return addressVal;
}


function createList(){
    var addresses = [];
    addresses.push(create("Y"));

    var randomBool = Math.floor(Math.random()*9) % 2;
    if(randomBool === 1){
        addresses.push(create("N"));
    }
    return addresses;
}

module.exports.create = create;
module.exports.createList = createList;