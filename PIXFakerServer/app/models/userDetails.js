var faker   = require('faker');
var country = require('./countries');
var lang    = require('./lang');

function UserDetails() {
    this.firstName = faker.name.firstName();
    this.lastName = faker.name.lastName();
    this.email = faker.internet.email(this.firstName, this.lastName);
    this.alternateEmail = Math.floor(Math.random() * 2) ? faker.internet.email(this.firstName, this.lastName) : null;
    this.phone = faker.phone.phoneNumber("##########");
    this.alternatePhone = Math.floor(Math.random() * 2) ? faker.phone.phoneNumber("##########") : null;
    this.address1 = faker.address.streetAddress();
    this.address2 = faker.address.secondaryAddress();
    this.city = faker.address.city();
    this.country = country.getCountry();
    this.postalCode = faker.address.zipCode().substring(0,5);
    this.language = lang.getLang();
}

function create() {
    return new UserDetails();
}

module.exports.create = create;