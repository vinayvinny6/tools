function getCountry() {
    var countries = ["CN", "US", "CA", "MX"]; //list of usable countries
    var r = Math.floor(Math.random() * countries.length);

    return countries[r];
}

module.exports.getCountry = getCountry;