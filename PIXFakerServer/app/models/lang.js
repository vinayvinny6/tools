function getLang() {
    var languages = ["en", "es", "fr"]; //list of usable languages
    var r = Math.floor(Math.random() * languages.length);

    return languages[r];
}

module.exports.getLang = getLang;